# Current list of files in lib/ is found in liblist.txt
BUILD_SYSTEM := ../../../build/core
include $(BUILD_SYSTEM)/combo/HOST_linux-x86.mk
include $(BUILD_SYSTEM)/definitions.mk
include Android.mk

current_dir := $(dir $(abspath $(lastword $(makefile_list))))

.SECONDARY:

SRC := src
OUT := out
RES := res
ASSETS := assets
APK := messaging.apk

SOURCES := $(shell find $(SRC) -name '*.java')
TARGETS := $(subst $(SRC), $(OUT), $(SOURCES:.java=.class)) out/com/android/messaging/R.class $(shell find $(OUT) -name '*.class')


JAR := jar
JAVAC := javac
JAVAC_OUTPUT_OPTION := -d $(OUT)
COMPILE.java := $(JAVAC) $(JAVACFLAGS)

DEX := $(HOME)/.android/build-tools/28.0.3/dx --dex
DEXFLAGS := --incremental --min-sdk-version=24 --verbose --no-strict
DEX_OUTPUT_OPTION = --output=$@
COMPILE.dex := $(DEX) $(DEXFLAGS)

AAPT := aapt
AAPT_OUTPUT_OPTION = -F $@
PACKAGE.aapt := $(AAPT) package $(AAPTFLAGS)



export CLASSPATH := $(PWD)/lib:$(OUT):$(HOME)/.android/platforms/android-28/android.jar:../../../prebuilts/sdk/current/support/v4/android-support-v4.jar:../../../prebuilts/sdk/current/support/v7/recyclerview/libs/android-support-v7-recyclerview.jar:../../../prebuilts/sdk/current/support/v7/appcompat/libs/android-support-v7-appcompat.jar:../../../prebuilts/sdk/current/support/v7/appcompat/libs/android-support-v7-appcompat.jar:../../../prebuilts/devtools/tools/lib/guava-17.0.jar:../../../prebuilts/tools/common/m2/repository/junit/junit/4.12/junit-4.12.jar

.DEFAULT_GOAL := all
.DEFAULT: all
all: $(APK)
targets: $(TARGETS)
	@#echo UPDATED TARGETS: $?

$(APK): $(TARGETS)

%.dex: %.jar orig.jar
	$(COMPILE.dex) $(DEX_OUTPUT_OPTION) $^

orig.jar:
	@touch $@
	JARPATH=$(shell realpath $@) && \
	for file in ../../../prebuilts/sdk/current/support/v4/android-support-v4.jar ../../../prebuilts/sdk/current/support/v7/recyclerview/libs/android-support-v7-recyclerview.jar ../../../prebuilts/sdk/current/support/v7/appcompat/libs/android-support-v7-appcompat.jar ../../../prebuilts/sdk/current/support/v7/appcompat/libs/android-support-v7-appcompat.jar ../../../prebuilts/devtools/tools/lib/guava-17.0.jar ../../../prebuilts/tools/common/m2/repository/junit/junit/4.12/junit-4.12.jar; do \
	    TMPDIR=$$(mktemp -d) && \
	    unzip $$file -d $$TMPDIR && \
	    (cd $$TMPDIR && $(RM) -f $(TARGETS:$(OUT)/%.class='%'*.class)) && \
	    $(JAR) uvf $@ -C $$TMPDIR . ; \
	    $(RM) -r $$TMPDIR;\
	done
	$(JAR) uvf $@ lib

%.jar: $(TARGETS)
	@touch $@
	$(JAR) uvf $@ $(?:$(OUT)/%=-C $(OUT)/ '%')
	@(cd out/ &> /dev/null; find * -type f | sort) > .real.thing && zipinfo -1 classes.jar | sort > .fake.thing && comm -23 .real.thing .fake.thing | xargs -I {} touch '$(OUT)/{}'; $(RM) .real.thing .fake.thing
	$(MAKE) $@



$(OUT)/%$$*.class: $(OUT)/%.class
$(OUT)/%.class: $(OUT) | $(SRC)/%.java
	@:


SOURCES += R.java
out/com/android/messaging/R.class: R.java

$(OUT): $(SOURCES)
	@test -e $@ || mkdir $@
	$(COMPILE.java) $(JAVAC_OUTPUT_OPTION) $?
	@find out/ -regextype posix-extended -regex '.*/($(shell echo $(notdir $(?:.java=)) | tr ' ' '|'))(\$$.*)?\.class' -print0 | xargs -0 touch
	@touch $@

R.java: package.apk

package.apk: AndroidManifest.xml $(RES) $(ASSETS)
	aapt package \
	             -M $(word 1,$^) \
	             -S $(word 2,$^) \
	             -A $(word 3,$^) \
	             -J . \
	             -u \
	             --auto-add-overlay \
	             --version-name 1.0.001 \
	             --version-code 10001140\
	             -z \
	             --pseudo-localize\
	             -c en_US,en_US,cs_CZ,da_DK,de_AT,de_CH,de_DE,de_LI,el_GR,en_AU,en_CA,en_GB,en_NZ,en_SG,eo_EU,es_ES,fr_CA,fr_CH,fr_BE,fr_FR,it_CH,it_IT,ja_JP,ko_KR,nb_NO,nl_BE,nl_NL,pl_PL,pt_PT,ru_RU,sv_SE,tr_TR,zh_CN,zh_HK,zh_TW,am_ET,hi_IN,en_US,en_AU,en_IN,fr_FR,it_IT,es_ES,et_EE,de_DE,nl_NL,cs_CZ,pl_PL,ja_JP,zh_TW,zh_CN,zh_HK,ru_RU,ko_KR,nb_NO,es_US,da_DK,el_GR,tr_TR,pt_PT,pt_BR,sv_SE,bg_BG,ca_ES,en_GB,fi_FI,hi_IN,hr_HR,hu_HU,in_ID,iw_IL,lt_LT,lv_LV,ro_RO,sk_SK,sl_SI,sr_RS,uk_UA,vi_VN,tl_PH,ar_EG,fa_IR,th_TH,sw_TZ,ms_MY,af_ZA,zu_ZA,am_ET,en_XA,ar_XB,fr_CA,km_KH,lo_LA,ne_NP,si_LK,mn_MN,hy_AM,az_AZ,ka_GE,my_MM,mr_IN,ml_IN,is_IS,mk_MK,ky_KG,eu_ES,gl_ES,bn_BD,ta_IN,kn_IN,te_IN,uz_UZ,ur_PK,kk_KZ,sq_AL,gu_IN,pa_IN,be_BY,bs_BA,ast_ES,lb_LU,ku_IQ,normal \
	             --preferred-density xxhdpi\
	             -S ../../../frameworks/support/v7/appcompat/res\
	             -S ../../../frameworks/support/v7/recyclerview/res\
	             -S ../../../frameworks/opt/chips/res\
	             -S ../../../frameworks/opt/colorpicker/res\
	             -S ../../../frameworks/opt/photoviewer/res\
	             -S ../../../frameworks/opt/photoviewer/activity/res\
	             -I /home/msirabella/Documents/projects/featurefork/aosp/lineage_2_electric_boogaloo/out/target/common/obj/APPS/framework-res_intermediates/package-export.apk\
	             -I /home/msirabella/Documents/projects/featurefork/aosp/lineage_2_electric_boogaloo/out/target/common/obj/APPS/org.cyanogenmod.platform-res_intermediates/package-export.apk \
	             -I ../../../out/target/common/obj/JAVA_LIBRARIES/libphonenumber-platform_intermediates/classes-full-debug.jar \
	             --product default \
	             --skip-symbols-without-default-localization\
	             -F $@ 
	$(JAR) uvf $@ -C ../../../external/libphonenumber/carrier/src com/google/i18n/phonenumbers/carrier/data/
	$(JAR) uvf $@ -C ../../../external/libphonenumber/libphonenumber/src com/google/i18n/phonenumbers/data/
	$(JAR) uvf $@ -C ../../../external/libphonenumber/geocoder/src com/google/i18n/phonenumbers/geocoding/data/
	$(JAR) uvf $@ -C ../../../external/libphonenumber/geocoder/src com/google/i18n/phonenumbers/timezones/data/
	
%.apk.unsigned: classes.dex package.apk
	@test -e $@ || cp $(word 2,$^) $@
	$(JAR) uvf $@ $<

%.apk.signed: %.apk.unsigned
	/home/msirabella/.android/build-tools/28.0.3/apksigner sign --verbose --in $< --out $@ --key ../../../build/target/product/security/platform.pk8 --cert ../../../build/target/product/security/platform.x509.pem


%.apk: %.apk.signed
	cp $< $@


clean:
	$(RM) *.apk.* *.jar *.dex R.java *.apk jartmp*
	$(RM) -rf $(OUT)
